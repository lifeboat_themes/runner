#Runner

##Home Page

###First Banner
Up to 2 products can be shown in a carousel in the Home banner section of Runner, as soon as you open the home page. These can be set in the custom fields labelled:

**Home: Banner Product 1, Home: Banner Product 2**

###Display Collections
2 Collections can have 4 products each to be displayed in different parts of the home page. These collections can be chosen in the custom fields labelled:

**Home: Display Collection 1, Home: Display Collection 2**

_NOTE: Display Collection 1 is shown right after the Home banner, and Display collection 2 is shown after the featured collections._

###Featured Collections
You can have 4 different collections to be shown in a featured box, which is shown in between the Display collections. These collections can be set in the custom fields labelled:

**Home: Featured Collection 1, Home: Featured Collection 2, Home: Featured Collection 3, Home: Featured Collection 4**

###Second Banner
Another product banner can be shown in the home page after the **Display Collection 2** section. This product can be selected from the custom field labelled:

**Home: Banner Product 3**

###Product Grid section
3 collections can be shown in a Product Grid, which is 3 columns showing up to 4 products each.
These can be set in the custom fields labelled:

**Home: Product Grid Collection 1, Home: Product Grid Collection 2, Home: Product Grid Collection 3**

##Footer
###Footer: Menu
_Note: Submenus items will not be displayed_

##Contact Page
###Map Location
In the Contact page, you can have a map of the store displayed underneath the Contact form.
This can be done by going to the custom field labelled **Map Location** in the Design section and inputting the store name and full address in the field, in the format shown below.

**Store Name, Triq it-Triq, Location Post Code**

_NOTE: It is important to get these details exactly as they are shown on Google Maps._

##Product
###Product Banner Image
The product banner image is the image shown in the banner instead of the default product image, if the product is selected to be shown in a banner.

###Banner Heading
The banner heading is the text shown above the product name in the banner, if the product is chosen to be shown in a banner.

###Category
Each product can have a category, which is chosen from the drop down menu in the field **Category**. The picture of the category chosen will be displayed as a background in a banner, if the product is chosen as such.

##Collection
###Collection Subtitle
The collection subtitle is shown in various places around the website, such as in the featured collections section.

###Collection banner image
The collection banner image is the image shown in the banner instead of the default collection image, at the top of each collection's page.
