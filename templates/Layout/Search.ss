<% include Header %>
<main class="main">
    <div class="page-content mb-6 pb-6">
        <div class="container">
            <div class="toolbox-wrap">
                <aside class="sidebar sidebar-fixed shop-sidebar closed">
                    <div class="sidebar-overlay"></div>
                    <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    <div class="sidebar-content">
                        <div class="mb-0 mb-lg-3">
                            <div class="filter-actions">
                                <a href="#" class="sidebar-toggle-btn toggle-remain btn btn-sm btn-outline btn-rounded btn-primary">Filter<i class="d-icon-arrow-left"> </i></a>
                            </div>
                            <form method="get" role="form">
                                <div class="sticky-sidebar">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <h3 class="widget-title">Search</h3>
                                            <div class="widget-body">
                                                <input class="form-control" type="text" placeholder="Search"  value="$getSearchTerm" name="search" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="widget price-with-count">
                                                <h3 class="widget-title">Price</h3>
                                                <div class="widget-body filter-items filter-price">
                                                    <label for="price_min">Min $SiteSettings.CurrencySymbol()</label>
                                                    <div class="input-group">
                                                         <input class="form-control" type="number" name="price_min" data-role="min_val" value="$getOptions().PriceRange.Min" />
                                                    </div>
                                                    <label for="price_max">Max $SiteSettings.CurrencySymbol()</label>
                                                    <div class="input-group">
                                                        <input class="form-control" type="number" name="price_max" data-role="max_val" value="$getOptions().PriceRange.Max" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <% loop $getOptions().SearchFilters %>
                                            <div class="col-lg-4">
                                                <div class="widget widget-collapsible">
                                                    <h3 class="widget-title">$Name</h3>
                                                    <ul class="widget-body filter-items">
                                                        <% loop $SearchData %>
                                                            <li data-value="$ID" class="search-datum <% if $Selected %>active<% end_if %>">
                                                                <a href="#">$Value<span>($Matches)</span></a>
                                                                <input type="checkbox" name="search_data[]" value="$ID" style="display:none"
                                                                    id="$ID" <% if $Selected %>checked<% end_if %> />
                                                            </li>
                                                        <% end_loop %>
                                                    </ul>
                                                </div>
                                            </div>
                                        <% end_loop %>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-sm btn-primary btn-rounded">Filter</button>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <a href="$AbsoluteLink" class="btn btn-sm btn-rounded btn-outline-secondary">Clear</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </aside>
                <div class="toolbox sticky-toolbox sticky-content fix-top">
                    <div class="toolbox-left">
                        <a href="#"
                           class="toolbox-item left-sidebar-toggle btn btn-outline btn-primary btn-rounded btn-icon-left font-primary"><i
                                class="d-icon-filter-2"></i>Filter</a>
                    </div>
                    <div class="toolbox-right">
                        <div class="toolbox-item toolbox-sort select-box text-dark">
                            <label>Sort By:</label>
                            <select name="orderby" class="form-control">
                                <% loop $Top.getOptions().Sort %>
                                    <option value="$FilterLink" <% if $Selected %>selected="selected"<% end_if %>>
                                        $Option
                                    </option>
                                <% end_loop %>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row cols-sm-2 cols-md-3 cols-xl-4 product-wrapper">
                <% loop $PaginatedResults(12) %>
                    <% include ProductCard Product=$Me %>
                <% end_loop %>
            </div>
            <% if PaginatedResults(12).MoreThanOnePage %>
                <% with PaginatedResults(12) %>
                <nav class="toolbox toolbox-pagination">
                    <ul class="pagination">
                        <% if $NotFirstPage %>
                        <li class="page-item">
                            <a class="page-link page-link-prev" href="$PrevLink" aria-label="Previous" tabindex="-1"
                               aria-disabled="true">
                                <i class="d-icon-arrow-left"></i>Prev
                            </a>
                        </li>
                        <% else %>
                        <li class="page-item disabled">
                            <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
                               aria-disabled="true">
                                <i class="d-icon-arrow-left"></i>Prev
                            </a>
                        </li>
                        <% end_if %>
                        <% loop $PaginationSummary %>
                            <% if $CurrentBool %>
                                <li class="page-item active" aria-current="page"><a class="page-link" href="#">$PageNum</a>
                            <% else %>
                                <% if $Link %>
                                    <li class="page-item"><a class="page-link" href="$Link">$PageNum</a></li>
                                <% else %>
                                    <li class="page-item">...</li>
                                <% end_if %>
                            <% end_if %>
                        <% end_loop %>
                        <% if $NotLastPage %>
                        <li class="page-item">
                            <a class="page-link page-link-next" href="$NextLink" aria-label="Next">
                                Next<i class="d-icon-arrow-right"></i>
                            </a>
                        </li>
                        <% else %>
                        <li class="page-item disabled">
                            <a class="page-link page-link-next" href="#" aria-label="Next" tabindex="-1" aria-disabled="true">
                                Next<i class="d-icon-arrow-right"></i>
                            </a>
                        </li>
                        <% end_if %>
                    </ul>
                </nav>
                <% end_with %>
            <% end_if %>
        </div>
    </div>
</main>
