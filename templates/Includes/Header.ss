<header class="header <%if $Trans==1 %> header-transparent <% end_if %>">
    <div class="header-middle sticky-header fix-top sticky-content">
        <div class="container">
            <div class="header-left">
                <a href="#" class="mobile-menu-toggle">
                    <i class="d-icon-bars2"></i>
                </a>
                <a href="/" class="logo">
                    <img src="$SiteSettings.Logo.Fit(154,43).AbsoluteLink" alt="$SiteSettings.Title logo" width="$SiteSettings.Logo.Fit(154,43).getWidth" height="$SiteSettings.Logo.Fit(154,43).getHeight" />
                </a>
                <!-- End Logo -->
                <nav class="main-nav mr-4">
                    <% include MainMenu %>
                </nav>
                <span class="divider d-lg-show"></span>
                <!-- End Divider -->
                <div class="header-search hs-toggle d-lg-show">
                    <a href="#" class="search-toggle">
                        <i class="d-icon-search"></i>
                    </a>
                    <form action="/search" class="input-wrapper">
                        <input type="text" class="form-control" name="search" autocomplete="off"
                               placeholder="Search your keyword..." required />
                        <button class="btn btn-search" type="submit">
                            <i class="d-icon-search"></i>
                        </button>
                    </form>
                </div>
                <!-- End Header Search -->
            </div>
            <div class="header-right">
                <div class="header-search hs-toggle d-lg-none mr-4">
                    <a href="#" class="search-toggle">
                        <i class="d-icon-search"></i>
                    </a>
                    <form action="/search" class="input-wrapper">
                        <input type="text" class="form-control" name="search" autocomplete="off"
                               placeholder="Search your keyword..." required />
                        <button class="btn btn-search" type="submit">
                            <i class="d-icon-search"></i>
                        </button>
                    </form>
                </div>
                <a href="tel:$ContactPage.Tel" class="call d-lg-show mr-4">
                    <div class="icon-box-icon">
                        <i class="d-icon-phone"></i>
                    </div>
                    <div class="icon-box-content">
                        <span>$ContactPage.Tel</span>
                    </div>
                </a>
                <span class="divider"></span>
                <a href="/customer/profile" class="login-link pb-0 mr-4"><i class="d-icon-user"></i></a>

                <a href="$WishList.AbsoluteLink" class="wishlist">
                    <i class="d-icon-heart"></i>
                </a>

                <div class="dropdown cart-dropdown type2 cart-offcanvas mr-0">
                    <a href="#" class="cart-toggle link">
                        <i class="d-icon-bag"><span class="cart-count">$Cart.getItemCount(0)</span></i>
                    </a>
                    <div class="cart-overlay"></div>
                    <!-- End Cart Toggle -->
                    <div class="dropdown-box">
                        <div class="cart-header">
                            <h4 class="cart-title">Shopping Cart</h4>
                            <a class="btn btn-dark btn-link btn-icon-right btn-close">close<i
                                    class="d-icon-arrow-right"></i></a>
                        </div>
                        <div class="products scrollable">
                            <% loop $Cart.getItems('0') %>
                                <div class="product product-cart" data-role="product" data-id="$Product.ID" data-variant="$Variant.ID">
                                    <figure class="product-media">
                                        <a href="$Product.Link">
                                            <img src="$Product.Image.Link" alt="product" width="80"
                                                 height="88" />
                                        </a>
                                        <button class="btn btn-link btn-close" data-role="delete">
                                            <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                        </button>
                                    </figure>
                                    <div class="product-detail">
                                        <a href="$Product.Link" class="product-name">$Product.Title</a>
                                        <small class="text-muted d-block">$Variant</small>
                                        <div class="price-box">
                                            <span class="product-quantity">$Quantity</span>
                                            <span class="product-price">$Product.FormatPrice('SellingPrice', $Variant.ID)</span>
                                        </div>
                                    </div>
                                </div>
                            <% end_loop %>
                        </div>
                        <!-- End of Products  -->
                        <div class="cart-total">
                            <label>Subtotal:</label>
                            <span class="price">$Cart.FormatPrice('Subtotal', 1)</span>
                        </div>
                        <!-- End of Cart Total -->
                        <% if $Cart.canOrder() %>
                            <div class="cart-action">
                                <a href="$Cart.CheckoutLink" class="btn btn-dark"><span>Checkout</span></a>
                            </div>
                        <% else %>
                            You are below the minimum order cost of $Cart.FormatPrice('MinOrder')
                        <% end_if %>
                        <!-- End of Cart Action -->
                    </div>
                    <!-- End Dropdown Box -->
                </div>
            </div>
        </div>
    </div>
</header>